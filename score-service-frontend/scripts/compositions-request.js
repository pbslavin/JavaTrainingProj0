const xhr = new XMLHttpRequest();
xhr.open("Get", "http://localhost:7000/compositions");
xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
        var compositions = JSON.parse(xhr.responseText);
        for (let composition of compositions) {
            function addRow(tableId) {   
                let table = document.getElementById("compositions-table");
                let row = table.insertRow(-1);
                let titleCell = row.insertCell(0);
                let title = document.createTextNode(composition.title);
                titleCell.appendChild(title);
                let yearCell = row.insertCell(1);
                let year = document.createTextNode(composition.yearComposed);
                yearCell.appendChild(year);
                let genreCell = row.insertCell(2);
                let genre = document.createTextNode(composition.genre);
                genreCell.appendChild(genre);
                let composerCell = row.insertCell(3);
                let composer = document.createTextNode(composition.composerId);
                composerCell.appendChild(composer);
            }
            addRow('compositions-table');
        };
    }
}
xhr.send();