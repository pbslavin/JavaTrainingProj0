package dev.slavin.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.slavin.JavalinApp;
import dev.slavin.models.Composer;
import dev.slavin.models.User;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.GenericType;
import dev.slavin.JavalinApp;
import org.apache.http.auth.AUTH;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ComposerControllerIntegrationTest {
    private static JavalinApp app = new JavalinApp();

    private static final String AUTH_TOKEN = "Authorization";
    private static final String GENERAL = "general-auth-token";
    private static final String ADMIN = "admin-auth-token";
    private static final String PATH = "http://localhost:7000/composers";
    private static final String INVALID_COMPOSER = "That is not a valid composer.";
    private static final String UNAUTHORIZED = "You are unauthorized.";


    @BeforeAll
    static void startService(){
        app.start(7000);
    }

    @AfterAll
    static void stopService(){
        app.stop();
    }

    @Test
    void getAllComposersPermitsAnyAndFetchesList() {
        HttpResponse<List<Composer>> response = Unirest.get(PATH)
                .header(AUTH_TOKEN, GENERAL)
                .asObject(new GenericType<List<Composer>>() {});
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertTrue(response.getBody().size() > 0)
        );
    }

    @Test
    void getPermitsAnyAndFetchesComposer() {
        HttpResponse<Composer> response = Unirest.get(PATH + "/1")
                .header(AUTH_TOKEN, GENERAL)
                .asObject(new GenericType<Composer>() {});
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertNotNull(response.getBody())
        );
    }

    @Test
    void postProhibitsUnauthorized() {
        HttpResponse<String> response = Unirest.post(PATH)
                .header(AUTH_TOKEN, GENERAL)
                .asString();
        assertAll(
                () -> assertEquals( 401, response.getStatus()),
                () -> assertEquals( UNAUTHORIZED, response.getBody()));
    }

    @Test
    void postPermitsAuthorized() {
        HttpResponse<String> response = Unirest.post(PATH)
                .header(AUTH_TOKEN, ADMIN)
                .asString();
        assertAll(
                () -> assertEquals( 400, response.getStatus()),
                () -> assertEquals( INVALID_COMPOSER, response.getBody()));
    }

    @Test
    void putProhibitsUnauthorized() {
        HttpResponse<String> response = Unirest.put(PATH)
                .header(AUTH_TOKEN, GENERAL)
                .asString();
        assertAll(
                () -> assertEquals( 401, response.getStatus()),
                () -> assertEquals( UNAUTHORIZED, response.getBody()));
    }

    @Test
    void putPermitsAuthorized() {
        HttpResponse<String> response = Unirest.put(PATH + "/1")
                .header(AUTH_TOKEN, ADMIN)
                .asString();
        assertAll(
                () -> assertEquals( 400, response.getStatus()),
                () -> assertEquals( INVALID_COMPOSER, response.getBody()));
    }

    @Test
    void deleteProhibitsUnauthorized() {
        HttpResponse<String> response = Unirest.delete(PATH + "/0")
                .header(AUTH_TOKEN, GENERAL)
                .asString();
        assertAll(
                () -> assertEquals( 401, response.getStatus()),
                () -> assertEquals( UNAUTHORIZED, response.getBody()));
    }

    @Test
    void deletePermitsAuthorized() {
        HttpResponse<String> response = Unirest.delete(PATH + "/0")
                .header(AUTH_TOKEN, ADMIN)
                .asString();
        assertAll(
                () -> assertEquals( 204, response.getStatus()),
                () -> assertEquals( "", response.getBody()));
    }
}
